import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  list:Article[] = []
  

  constructor(private articleService: ArticleService) { }

  ngOnInit(): void {
    this.articleService.getAll().subscribe(data => {this.list = data; console.log(data); console.log(this.list)});
  }

}
