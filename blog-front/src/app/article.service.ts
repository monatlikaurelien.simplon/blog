import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Article } from './entities';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private http:HttpClient) { }

  getAll() {
    return this.http.get<Article[]>(environment.apiUrl+'/api/article');
  }

  getById(id:number) {
    return this.http.get<Article>(environment.apiUrl+'/api/article/'+id);
  }

  add(operation:Article){
    return this.http.post<Article>(environment.apiUrl+'/api/artcile', operation);
  }

  delete(id:number) {
    return this.http.delete(environment.apiUrl+'/api/artcile/'+id);
  }

  put(operation:Article){
    return this.http.put<Article>(environment.apiUrl+'/api/article/'+operation.id, operation);
  }

}
