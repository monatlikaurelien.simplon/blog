

DROP TABLE IF EXISTS article;

CREATE TABLE article 
(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(128),
    date DATE NOT NULL,
    content TEXT NOT NULL
) DEFAULT CHARSET UTF8;

INSERT INTO article (title, date, content) VALUES 
("Saint Exupery", 
"2018-05-08",
"Départ de l'aéroport de Saint Exepury de Lyon. Pour mon premier voyage je suis accompagnée de ma soeur, de sa copine puis de leurs amis. Face a moi une nouvelle aventure commence, à lage de 19 ans, équiper de mes affaires et de ma cigarette tel un beauf. Je suis enfin pret à affronter le continent asiatique."),

("Arrivée Tokyo",
 "2018-05-09",
 "Aprés plus de 10h de vol je suis enfin arrivé à Tokyo. Quelle belle ville."),

 ("Restaurant", 
 "2018-05-10",
 "C'était vraiment trés bon.");

--  INSERT INTO article (title, date, content) VALUES ("3","2022-01-01","5")

