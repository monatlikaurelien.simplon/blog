package co.simplon.promo18.blog.entity;
import java.time.LocalDate;

public class Article {
  private Integer id;
  private String title;
  private LocalDate date;
  private String content;
  public Article() {}
  public Article(String title, LocalDate date, String content) {
    this.title = title;
    this.date = date;
    this.content = content;
  }
  public Article(Integer id, String title, LocalDate date, String content) {
    this.id = id;
    this.title = title;
    this.date = date;
    this.content = content;
  }
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  public LocalDate getDate() {
    return date;
  }
  public void setDate(LocalDate date) {
    this.date = date;
  }
  public String getContent() {
    return content;
  }
  public void setContent(String content) {
    this.content = content;
  }
}

