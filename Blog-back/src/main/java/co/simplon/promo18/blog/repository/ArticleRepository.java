package co.simplon.promo18.blog.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.blog.entity.Article;


@Repository
public class ArticleRepository {
  @Autowired
  private DataSource dataSource;

  public List<Article> findAll() {
    List<Article> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {


        list.add(sqlToArtcile(rs));
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

    return list;
  }

  public Article findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article WHERE id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();


      if (rs.next()) {
        return sqlToArtcile(rs);

      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
    return null;
  }

  public void save(Article article) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("INSERT INTO article (title, date, content) VALUES (?,?,?)",
              PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, article.getTitle());
      stmt.setDate(2, Date.valueOf(article.getDate()));
      stmt.setString(3, article.getContent());

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        article.setId(rs.getInt(1));
      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
  }

  public boolean delete(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM article WHERE id=?");
      stmt.setInt(1, id);

      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
  }

  public boolean update(Article article) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection
          .prepareStatement("UPDATE article SET title = ?, date = ?, content = ? WHERE id = ?");

      stmt.setString(1, article.getTitle());
      stmt.setDate(2, java.sql.Date.valueOf(article.getDate()));
      stmt.setString(3, article.getContent());

      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

  }

  private Article sqlToArtcile(ResultSet rs) throws SQLException {
    return new Article(rs.getInt("id"), rs.getString("title"), rs.getDate("date").toLocalDate(),
        rs.getString("content"));
  }


}

