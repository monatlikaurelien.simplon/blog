package co.simplon.promo18.blog.controller;

import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import co.simplon.promo18.blog.entity.Article;
import co.simplon.promo18.blog.repository.ArticleRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/article")
public class ArticleController {

  @Autowired
  private ArticleRepository artRepo;

  @GetMapping
  public List<Article> getAll() {
      return artRepo.findAll();
  }

  @PostMapping
  public Article add(@RequestBody Article article) {
      if (article.getDate() == null) {
          article.setDate(LocalDate.now());
      }

      artRepo.save(article);
      return article;
  }
  
}
